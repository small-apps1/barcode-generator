const apiEndPoint = {
    "urlCheckBarcode": "/api/check-barcode/",
    "urlUpdateBarcode": "/api/update-barcode/",
    "UrlScanned": "/api/scan/",
    "UrlReport": "/api/report/",
    "UrlGetSes": "/api/get-ses/"
}


function GenerateBarcode(list_barcode) {
    const containerBarcode = document.getElementById("container-barcode")
    for (let i = 0; i < list_barcode.length; i++) {
        const noMes = list_barcode[i].no_mesin
        const code = list_barcode[i].code
        const printed = list_barcode[i].printed
        const scanned = list_barcode[i].scanned

        const dataBarcode = {
            idWR: noMes + Math.floor(Math.random() * 10000),
            idBarcode: noMes + code,
            idImgBarcode: noMes + i,
            p: printed ? printed : "-",
            s: scanned ? scanned : "-",
            noMes: noMes,
            b1: code[0] + code[1] + code[2],
            b2: code[3] + code[4] + code[5]
        }
        containerBarcode.innerHTML += createTmplBarcodeStr(dataBarcode)
        JsBarcode(`#${dataBarcode.idImgBarcode}`, dataBarcode.idBarcode, {
            width: 1,
            height: 40,
            margin: 0,
            displayValue: false
        });
    }
}