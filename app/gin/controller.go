package gin

import (
	"barcode/models/entities"
	"barcode/models/web"
	"barcode/service"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func NewGinConntroller(helper *GinHelper, service *service.ServiceBarcode) *GinConntroller {
	return &GinConntroller{
		helper:  helper,
		service: service,
	}
}

type GinConntroller struct {
	helper  *GinHelper
	service *service.ServiceBarcode
}

func (g *GinConntroller) Home(ctx *gin.Context) {
	ctx.HTML(200, "index.html", nil)
}

func (g *GinConntroller) Preview(ctx *gin.Context) {
	ctx.HTML(200, "preview.html", nil)
}

func (g *GinConntroller) PreviewBarcode(ctx *gin.Context) {
	listBarcode := ctx.Query("lb")
	decodedData, err := base64.StdEncoding.DecodeString(listBarcode)

	if err != nil {
		fmt.Println("Error decoding:", err)
		ctx.HTML(200, "preview-barcode.html", nil)
		return
	}

	log.Print(decodedData)
	ctx.HTML(200, "preview-barcode.html", nil)
}

func (g *GinConntroller) CheckBarcode(ctx *gin.Context) {
	request := &web.DataBarcode{}
	g.helper.ReadRequestBody(ctx, request)
	response := &web.ResponseCheckBarcode{}
	g.service.CheckBarcode(ctx, request, response)

	// set session
	session := sessions.Default(ctx)
	jbrc, _ := json.Marshal(response.Data)
	session.Set("barcodes", string(jbrc))
	session.Save()
	ctx.JSON(response.Code, response)
}

func (g *GinConntroller) UpdateBarcode(ctx *gin.Context) {
	request := &web.DataBarcode{}
	g.helper.ReadRequestBody(ctx, request)
	response := &web.ResponseUpdateBarcode{}
	g.service.UpdateBarcode(ctx, request, response)
	ctx.JSON(response.Code, response)
}

func (g *GinConntroller) ReportPeriode(ctx *gin.Context) {
	request := &web.ReportRequestBody{}
	g.helper.ReadRequestBody(ctx, request)
	response := &web.ResponseCheckBarcode{}
	g.service.GetBarcodeByPeriode(ctx, request, response)
	ctx.JSON(response.Code, response)
}

func (g *GinConntroller) GetSes(ctx *gin.Context) {
	response := &web.ResponseCheckBarcode{}
	parseData := []*entities.Barcode{}
	session := sessions.Default(ctx)
	ses := session.Get("barcodes")
	str := fmt.Sprintf("%v", ses)
	json.Unmarshal([]byte(str), &parseData)
	response.Data = parseData
	ctx.JSON(response.Code, response)
}
