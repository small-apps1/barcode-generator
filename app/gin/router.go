package gin

import (
	"barcode/models/constants"
	"barcode/service"
	"os"

	g "github.com/gin-gonic/gin"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"

	"go.uber.org/zap"
)

func setupGinEngine() *g.Engine {
	if os.Getenv("ENVIROTMENT") == "PRODUCTION" {
		g.SetMode(g.ReleaseMode)
	}
	gin := g.Default()
	gin.LoadHTMLGlob("./views/*.html")
	return gin
}

func Router(l *zap.Logger, s *service.ServiceBarcode) *g.Engine {
	r := setupGinEngine()
	h := NewGinHelper(l)
	m := NewGinMiddleware(l)
	c := NewGinConntroller(h, s)

	// middleware
	r.Use(m.Middleware)

	// serve static
	r.Static("/static/", "./static/")

	store := cookie.NewStore([]byte("abc12345#"))
	r.Use(sessions.Sessions("mysession", store))

	// controller
	r.GET(constants.UrlHome, c.Home)
	r.GET(constants.UrlPreview, c.Preview)
	r.GET(constants.UrlPreviewBarcode, c.PreviewBarcode)
	r.POST(constants.UrlCheckBarcode, c.CheckBarcode)   // Parsing form generate barcode
	r.POST(constants.UrlUpdateBarcode, c.UpdateBarcode) // Hit when Printing (All & New)
	r.POST(constants.UrlScanned, c.UpdateBarcode)       // Hit when scaning
	r.POST(constants.UrlReport, c.ReportPeriode)
	r.POST(constants.UrlSes, c.GetSes) // Hit report YYYY-mm-dd

	return r
}
