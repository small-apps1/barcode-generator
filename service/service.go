package service

import (
	"barcode/db"
	"barcode/models/entities"
	"barcode/models/web"
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

func NewServiceBarcode(logger *zap.Logger, repo *db.BarcodeRepository) *ServiceBarcode {
	return &ServiceBarcode{
		logger: logger,
		repo:   repo,
	}
}

type ServiceBarcode struct {
	logger *zap.Logger
	repo   *db.BarcodeRepository
}

func (s *ServiceBarcode) CheckBarcode(ctx context.Context, req *web.DataBarcode, res *web.ResponseCheckBarcode) {
	res.Data = []*entities.Barcode{}

	for _, dbc := range req.ListBarcode {
		oldCode, err := s.repo.CheckBarcode(ctx, dbc.NoMesin, dbc.Code)

		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				res.Data = append(res.Data, &entities.Barcode{
					NoMesin: dbc.NoMesin,
					Code:    dbc.Code,
					Printed: false,
					Scanned: false,
				})
			}

			continue
		}
		if oldCode.Printed {
			result := entities.Barcode{
				NoMesin: dbc.NoMesin,
				Code:    dbc.Code,
				Printed: oldCode.Printed,
				Scanned: oldCode.Scanned,
			}
			res.Data = append(res.Data, &result)
		} else {
			result := entities.Barcode{
				NoMesin: dbc.NoMesin,
				Code:    dbc.Code,
				Printed: false,
				Scanned: false,
			}
			res.Data = append(res.Data, &result)
		}
	}

	res.Code = 200
	res.Msg = "OK"
}

func (s *ServiceBarcode) UpdateBarcode(ctx context.Context, req *web.DataBarcode, res *web.ResponseUpdateBarcode) {
	var dataBarcodeForInsert []*entities.Barcode
	inserDatasBulks := []*entities.Barcode{}
	for _, dbc := range req.ListBarcode {
		switch req.PrintType {
		case 1:
			if dbc.Printed {
				err := s.repo.UpdateBarcode(ctx, dbc.NoMesin, dbc.Code, false)
				if err != nil {
					fmt.Println("#UpdateBarcode ERROR LOG", err.Error())
					continue
				}

				brc, err := s.repo.CheckBarcode(ctx, dbc.NoMesin, dbc.Code) // get lates data bacode
				if err != nil {
					fmt.Println("#UpdateBarcode ERROR LOG", err.Error())
					inserData := entities.Barcode{
						NoMesin: dbc.NoMesin,
						Code:    dbc.Code,
						Printed: false,
						Scanned: false,
						UAt:     time.Now(),
						CAt:     time.Now(),
					}
					dataBarcodeForInsert = append(dataBarcodeForInsert, &inserData)
					continue
				}

				dataBarcodeForInsert = append(dataBarcodeForInsert, &brc)
			} else {
				inserData := entities.Barcode{
					NoMesin: dbc.NoMesin,
					Code:    dbc.Code,
					Printed: true,
					Scanned: false,
					UAt:     time.Now(),
					CAt:     time.Now(),
				}
				inserDatasBulks = append(inserDatasBulks, &inserData)
				// Append
				dataBarcodeForInsert = append(dataBarcodeForInsert, &inserData)
			}
		case 2: // TODO Scan
			err := s.repo.UpdateBarcode(ctx, dbc.NoMesin, dbc.Code, true)
			if err != nil {
				fmt.Println("#UpdateBarcode ERROR LOG", err.Error())
				continue
			}
			inserData := entities.Barcode{
				NoMesin: dbc.NoMesin,
				Code:    dbc.Code,
				Printed: true,
				Scanned: true,
				UAt:     time.Now(),
				CAt:     time.Now(),
			}

			dataBarcodeForInsert = append(dataBarcodeForInsert, &inserData)
		}
	}

	// bulks insert
	if len(inserDatasBulks) > 0 {
		s.repo.InsertBarcode(ctx, inserDatasBulks)
	}

	res.Data = dataBarcodeForInsert
	res.Code = 200
	res.Msg = "OK"
}

func (s *ServiceBarcode) GetBarcodeByPeriode(ctx context.Context, req *web.ReportRequestBody, res *web.ResponseCheckBarcode) {
	barcodes, err := s.repo.GetBarcodeByPeriode(ctx, req.NoMesin, req.StartDate, req.EndDate)
	if err != nil {
		res.Code = 400
		res.Msg = "Error " + err.Error()
		return
	}
	res.Data = barcodes
	res.Code = 200
	res.Msg = "OK"
}

func generatorBarcode() {
	// PAD STAR

	//loop
	// check ba

	//
	// Seve sessions

}
