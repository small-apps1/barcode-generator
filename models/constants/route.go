package constants

// url end point
const (
	UrlHome             = "/"
	UrlPreviewBarcode   = "/preview-barcode/"
	UrlPreview          = "/preview/"
	UrlCheckBarcode     = "/api/check-barcode/"
	UrlUpdateBarcode    = "/api/update-barcode/"
	UrlAvailableBarcode = "/api/available-barcode/"
	UrlScanned          = "/api/scan/"
	UrlReport           = "/api/report/"
	UrlSes              = "/api/get-ses/"
)
