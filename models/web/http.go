package web

import "barcode/models/entities"

type ItemsBarcode struct {
	NoMesin string `json:"no_mesin"`
	Code    string `json:"code"`
	Printed bool   `json:"printed"`
	Scanned bool   `json:"scanned"`
}

type DataBarcode struct {
	PrintType   int            `json:"print_type"` // 1 Print,  2 scanned
	ListBarcode []ItemsBarcode `json:"list_barcode"`
}

type ResponseCheckBarcode struct {
	Code int                 `json:"code"`
	Msg  string              `json:"msg"`
	Ses  string              `json:"ses"`
	Data []*entities.Barcode `json:"data_barcode"`
}

type ResponseUpdateBarcode struct {
	Code int                 `json:"code"`
	Msg  string              `json:"msg"`
	Data []*entities.Barcode `json:"data_barcode"`
}

type ResponseUpdateAvailableBarcode struct {
	Code int                 `json:"code"`
	Msg  string              `json:"msg"`
	Data []*entities.Barcode `json:"data_barcode"`
}

type ReportRequestBody struct {
	NoMesin   string `json:"no_mesin"`
	StartDate string `json:"start_date"`
	EndDate   string `json:"end_date"`
}
