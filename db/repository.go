package db

import (
	"barcode/models/entities"
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

func NewBarcodeRepository(dbConn *gorm.DB, logger *zap.Logger) *BarcodeRepository {
	return &BarcodeRepository{
		logger: logger,
		db:     dbConn,
	}
}

type BarcodeRepository struct {
	logger *zap.Logger
	db     *gorm.DB
}

func (c *BarcodeRepository) CheckBarcode(ctx context.Context, noMesin, code string) (b entities.Barcode, err error) {
	var dataBarcode entities.Barcode

	result := c.db.Where("no_mesin = ? and code = ?", noMesin, code).
		First(&dataBarcode)
	if result.Error != nil {
		return dataBarcode, result.Error
	}
	return dataBarcode, nil
}

func (c *BarcodeRepository) GetBarcodeByPeriode(ctx context.Context, noMesin, startDate, endDate string) (b []*entities.Barcode, err error) {
	dataBarcode := []*entities.Barcode{}

	result := c.db
	if noMesin != "" {
		result = result.Where("no_mesin = ? AND (date(u_at) >= ? and date(u_at) <= ?) ", noMesin, startDate, endDate)
	} else {
		result = result.Where("date(u_at) >= ? and date(u_at) <= ?", noMesin, startDate, endDate)
	}

	result = result.Find(&dataBarcode)

	if result.Error != nil {
		return dataBarcode, result.Error
	}
	return dataBarcode, nil
}

func (c *BarcodeRepository) InsertBarcode(ctx context.Context, listBarcode []*entities.Barcode) {
	insertData := c.db.Create(listBarcode)
	if insertData.Error != nil {
		fmt.Println("Error", insertData.Error.Error())
	}
}

func (c *BarcodeRepository) UpdateBarcode(ctx context.Context, noMesin, barcode string, isScanned bool) (err error) {
	var bcrc entities.Barcode
	updateBrc := entities.Barcode{
		UAt: time.Now(),
		CAt: time.Now(),
	}
	if isScanned {
		updateBrc.Scanned = isScanned
	}

	err = c.db.Model(&bcrc).
		Where("no_mesin = ? and code = ? ", noMesin, barcode).
		Updates(updateBrc).
		Error
	return
}

func (c *BarcodeRepository) DeleteBarcode(ctx context.Context, noMesin, barcode string) (err error) {
	err = c.db.
		Where("no_mesin = ?", noMesin).
		Where("code = ?", barcode).
		Delete(&entities.Barcode{}).
		Error
	return
}
